package by.me.xo.frames;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import by.me.xo.db.WorkDB;
import by.me.xo.frames.RegFrame;
public class Log1Frame extends JFrame {
	
	private JPanel panel;
	private JTextField login;
	private JPasswordField password;
	private JLabel labelLogin, labelPassword;
	private JButton create;
	
	public Log1Frame() {
		setTitle("XO");
		setSize(240, 160);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		initComponents();
		action();
		setVisible(true);
	}
	
	public void initComponents() {
		panel = new JPanel();
		
		login = new JTextField("", 20);
		
		password = new JPasswordField("", 20);
		
		labelLogin = new JLabel("login");
		labelPassword = new JLabel("pass");
		
		create = new JButton("Log in");
	
		panel.add(labelLogin);
		panel.add(login);
		panel.add(labelPassword);
		panel.add(password);
		panel.add(create);
		
		add(panel);
	}
	
	public void action() {
		create.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!login.getText().isEmpty() && !String.valueOf(password.getPassword()).isEmpty()) {
					//WorkDB.checklogins(login.getText(), String.valueOf(password.getPassword()));
					dispose();
					new GameFrame();
				}
			}
			
		});
		
	}
}
