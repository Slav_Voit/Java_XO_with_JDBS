package by.me.xo.frames;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import by.me.xo.db.WorkDB;
import by.me.xo.frames.RegFrame;

public class Frame1or2 extends JFrame{
	
	private JPanel panel;
	
	private JLabel labelNum, labelRegistry;
	private JButton create1,create2, Registry;
	
	public Frame1or2() {
		setTitle("XO");
		setSize(135, 150);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		initComponents();
		action();
		setVisible(true);
	}
	
	public void initComponents() {
		panel = new JPanel();
			
		labelNum = new JLabel("How many players?");
		labelRegistry = new JLabel("???Registry???");
		
		create1 = new JButton("1");
		create2 = new JButton("2");
		Registry = new JButton("sign up");
		
		panel.add(labelNum);
		panel.add(create1);
		panel.add(create2);
		panel.add(labelRegistry);
		panel.add(Registry);
		
		add(panel);
	}
	
	public void action() {
		create1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				new Log1Frame();
			}
			
		});
		
		create2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				new Log2Frame();
			}
			
		});
		
		Registry.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				new RegFrame();
			}
			
		});
	}
}
