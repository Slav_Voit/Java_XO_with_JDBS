package by.me.xo.frames;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import by.me.xo.db.WorkDB;
import by.me.xo.frames.RegFrame;
public class Log2Frame extends JFrame {
	
	private JPanel panel;
	private JTextField login1,login2;
	private JPasswordField password1,password2;
	private JLabel labelLogin1, labelPassword1,labelLogin2, labelPassword2;
	private JButton create;
	
	public Log2Frame() {
		setTitle("XO");
		setSize(480, 160);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		initComponents();
		action();
		setVisible(true);
	}
	
	public void initComponents() {
		panel = new JPanel();
		
		login1 = new JTextField("", 20);
		
		password1 = new JPasswordField("", 20);
		
		labelLogin1 = new JLabel("The first login                                          ");
		labelPassword1 = new JLabel("The first password                                       ");
		
		login2 = new JTextField("", 20);
		
		password2 = new JPasswordField("", 20);
		
		labelLogin2 = new JLabel("The second login");
		labelPassword2 = new JLabel("The second password");
		
		create = new JButton("Log in");
	
		panel.add(labelLogin1);
		panel.add(labelLogin2);
		panel.add(login1);
		panel.add(login2);
		panel.add(labelPassword1);
		panel.add(labelPassword2);
		panel.add(password1);
		panel.add(password2);
		panel.add(create);
		
		add(panel);
	}
	
	public void action() {
		create.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!login1.getText().isEmpty() && !String.valueOf(password1.getPassword()).isEmpty() && !login2.getText().isEmpty() && !String.valueOf(password2.getPassword()).isEmpty()) {
					//WorkDB.checklogins(login.getText(), String.valueOf(password.getPassword()));
					dispose();
					new GameFrame();
				}
			}
			
		});
		
	}
}
