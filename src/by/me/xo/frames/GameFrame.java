package by.me.xo.frames;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;

import by.me.xo.db.WorkDB;
import by.me.xo.frames.Log1Frame;
import by.me.xo.frames.Log2Frame;
public class GameFrame extends JFrame{

	private JPanel panel;
	private JButton [] mas = new JButton[9];
	private static boolean im = false;//������ ���������� ������ ����� �������� �������������
	
	public GameFrame() {
		setTitle("XO");
		setSize(400, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		initComponents();
		action();
		setVisible(true);
	}
	
	public void initComponents() {
		panel = new JPanel();
		panel.setLayout(new GridLayout(3, 3));
		
		for(int i = 0; i < mas.length; i++) {
			mas[i] = new JButton();
			panel.add(mas[i]);
		}
		/*mas[0].setIcon(new ImageIcon("S://123.jpg"));
		mas[1].setIcon(new ImageIcon("S://1234.jpg"));*/
		
		add(panel);
	}
	
	public static void changeImage(JButton button) {
		if(button.getIcon()==null) {
			if(im) {
				
				button.setIcon(new ImageIcon("C://Users//Slava//Desktop//XXXO (2)//XO//cross.png"));
				im = false;
			}else {
				button.setIcon(new ImageIcon("C://Users//Slava//Desktop//XXXO (2)//XO//null.png"));
				im = true;
			}
		}
	}
	
	public void action() {
		ActionListener al = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String line = e.getSource().toString();
				String finalLine = line.substring(line.indexOf(",") + 1, line.indexOf(",", line.indexOf(",") + 1)) 
						+ line.substring(line.indexOf(",", line.indexOf(",") + 1) + 1, line.indexOf(",", line.indexOf(",", line.indexOf(",") + 1)+1));
				//System.out.println(finalLine);
				switch(finalLine) {
				case "01":
					changeImage(mas[0]);
					break;
				case "1311":
					changeImage(mas[1]);
					break;
				case "2621":
					changeImage(mas[2]);
					break;
				case "0124":
					changeImage(mas[3]);
					break;
				case "131124":
					changeImage(mas[4]);
					break;
				case "262124":
					changeImage(mas[5]);
					break;
				case "0247":
					changeImage(mas[6]);
					break;
				case "131247":
					changeImage(mas[7]);
					break;
				case "262247":
					changeImage(mas[8]);
					break;
					
			
				}
			}
		};
		for(int i = 0; i < mas.length; i++) {
			mas[i].addActionListener(al);
		}
		
	}
}
